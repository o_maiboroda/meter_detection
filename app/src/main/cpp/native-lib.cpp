#include <jni.h>
#include <string>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>

using namespace std;
using namespace cv;

extern "C"
{
void JNICALL Java_o_maiboroda_tesstest_MainActivity_detect(JNIEnv *env, jobject instance,
                                                                           Mat src,
                                                                           jint nbrElem) {
}
}
extern "C" JNIEXPORT jstring JNICALL Java_o_maiboroda_tesstest_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

