package o.maiboroda.tesstest.recognizer

import android.content.Context
//import android.graphics.Bitmap
//import org.tensorflow.contrib.android.TensorFlowInferenceInterface
//import java.io.BufferedReader
//import java.io.IOException
//import java.io.InputStreamReader
//import java.util.*


class TensorflowHelper(context: Context) {
//    private val labels = Vector<String>()
//    private var numberOfClasses = 0
//    private var tensorflow: TensorFlowInferenceInterface? = null
//    init {
//        tensorflow = TensorFlowInferenceInterface(context.assets, MODEL_FILE)
//
//        loadLabels(context)
//    }
//
//    fun extractText(bitmap: Bitmap): String {
//
//        val resizedBitmap = Bitmap.createScaledBitmap(bitmap, INPUT_SIZE, INPUT_SIZE, false)
//
//        val outputNames = arrayOf(OUTPUT_NAME)
//        val intValues = IntArray((INPUT_SIZE * INPUT_SIZE))
//        val floatValues = FloatArray((INPUT_SIZE * INPUT_SIZE * 3))
//        val outputs = FloatArray(numberOfClasses)
//
//        resizedBitmap.getPixels(intValues, 0, resizedBitmap.width, 0, 0, resizedBitmap.width, resizedBitmap.height)
//
//        for (i in 0 until intValues.size) {
//            val `val` = intValues[i]
//            floatValues[i * 3 + 0] = (`val` and 0xFF).toFloat()
//            floatValues[i * 3 + 1] = (`val` shr 8 and 0xFF).toFloat()
//            floatValues[i * 3 + 2] = (`val` shr 16 and 0xFF).toFloat()
//        }
//
//        tensorflow?.feed(INPUT_NAME, floatValues, 1, INPUT_SIZE.toLong(), INPUT_SIZE.toLong(), 3)
//        tensorflow?.run(outputNames)
//        tensorflow?.fetch(OUTPUT_NAME, outputs)
//
//        var maxIndex = -1
//        var maxConf = 0f
//
//        for (i in 0 until outputs.size) {
//            if (outputs[i] > maxConf) {
//                maxConf = outputs[i]
//                maxIndex = i
//            }
//        }
//
//        return labels[maxIndex]
//    }
//
//    private fun loadLabels(context: Context) {
//        val assetManager = context.assets
//
//        // loading labels
//        val br: BufferedReader?
//        try {
//            val inputStream = assetManager.open("labels.txt")
//            br = BufferedReader(InputStreamReader(inputStream))
//            var line: String?
//            line = br.readLine()
//            while (line != null) {
//                labels.add(line)
//                line = br.readLine()
//            }
//            br.close()
//
//            numberOfClasses = labels.size
//        } catch (e: IOException) {
//            throw RuntimeException("error reading labels file!", e)
//        }
//    }
//
//    companion object {
//        private const val MODEL_FILE = "file:///android_asset/model.pb"
//        private const val INPUT_SIZE = 227
//        private const val INPUT_NAME = "Placeholder"
//        private const val OUTPUT_NAME = "loss"
//    }
}
