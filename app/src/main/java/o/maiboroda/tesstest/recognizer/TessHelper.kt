package o.maiboroda.tesstest.recognizer

import android.content.res.AssetManager
import android.graphics.Bitmap
import android.os.Environment
import android.text.TextUtils
import android.util.Log
import com.googlecode.tesseract.android.TessBaseAPI
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class TessHelper {

    fun extractText(bitmap: Bitmap): String {
        val tessBaseApi = TessBaseAPI()
        tessBaseApi.init(DATA_PATH, "eng")
        tessBaseApi.pageSegMode = TessBaseAPI.PageSegMode.PSM_SINGLE_CHAR
        tessBaseApi.setImage(bitmap)
        tessBaseApi.setVariable("tessedit_char_whitelist", "0123456789")
        var extractedText = tessBaseApi.utF8Text
        val confidence = tessBaseApi.resultIterator.confidence(TessBaseAPI.PageIteratorLevel.RIL_SYMBOL)
        tessBaseApi.end()
        Log.d("tag", "$extractedText confidence - $confidence")
        if (TextUtils.isEmpty(extractedText)) extractedText = "?"
        return extractedText ?: "0"
    }

    fun prepareTesseract(assets: AssetManager) {
        try {
            prepareDirectory(DATA_PATH + TESSDATA)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        copyTessDataFiles(assets, TESSDATA)
    }

    private fun prepareDirectory(path: String) {
        val dir = File(path)
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                Log.e(TAG, "ERROR: Creation of directory $path failed, check does Android Manifest have permission to write to external storage.")
            }
        } else {
            Log.i(TAG, "Created directory $path")
        }
    }

    /**
     * Copy tessdata files (located on assets/tessdata) to destination directory
     *
     * @param path - name of directory with .traineddata files
     */
    private fun copyTessDataFiles(assets: AssetManager, path: String) {
        try {
            val fileList = assets.list(path)

            for (fileName in fileList) {
                val pathToDataFile = "$DATA_PATH$path/$fileName"
                if (!File(File(File(Environment.getExternalStorageDirectory(), "TesseractSample"), path), fileName).exists()) {

                    val `in` = assets.open("$path/$fileName")

                    val out = FileOutputStream(pathToDataFile)

                    // Transfer bytes from in to out
                    val buf = ByteArray(1024)
                    var len: Int = `in`.read(buf)

                    while (len > 0) {
                        out.write(buf, 0, len)
                        len = `in`.read(buf)
                    }
                    `in`.close()
                    out.close()

                    Log.d(TAG, "Copied " + fileName + "to tessdata")
                }
            }
        } catch (e: IOException) {
            Log.e(TAG, "Unable to copy files to tessdata " + e.toString())
        }
    }

    companion object {
        private const val TAG = "tag"
        private val DATA_PATH = Environment.getExternalStorageDirectory().toString() + "/TesseractSample/"
        private const val TESSDATA = "tessdata"
    }
}