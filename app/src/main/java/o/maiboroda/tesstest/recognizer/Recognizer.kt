package o.maiboroda.tesstest.recognizer

import android.app.Activity
import android.graphics.Bitmap
import android.util.Log
import o.maiboroda.tesstest.utils.PermissionChecker
import org.opencv.android.Utils
import org.opencv.core.Mat
import org.opencv.core.Rect

class Recognizer(private val activity: Activity) {
    private val helper by lazy { TessHelper() }
//    private val helper by lazy { TensorflowHelper(activity) }

    fun subscribe() {
        if (!PermissionChecker.checkPermissions(activity)) {
            helper.prepareTesseract(activity.assets)
        }
    }

    fun recognize(src: Mat, rects: MutableList<Rect>?, border: Int): ArrayList<Int> {
        rects?.sortBy { it.x }

        val array = ArrayList<Int>(6)
        rects?.forEachIndexed { index, it ->
            it.x += border / 2
            it.y += border / 2
            val mat = Mat(src, it)
            val bitmap = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888)
            Utils.matToBitmap(mat, bitmap)

            array.add(try {
                helper.extractText(bitmap).toInt()
            } catch (e: Exception) {
                Log.d("tag", e.message)
                0
            })
        }
        return array
    }
}