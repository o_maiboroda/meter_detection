package o.maiboroda.tesstest.utils

import android.content.Context
import android.hardware.Camera
import android.util.AttributeSet
import org.opencv.android.JavaCameraView

class CameraView(context: Context, attrs: AttributeSet) : JavaCameraView(context, attrs) {

    fun enableFlashlight(isEnable: Boolean) {
        mCamera.parameters = mCamera.parameters
                .apply {
                    flashMode = if (isEnable) Camera.Parameters.FLASH_MODE_TORCH
                    else Camera.Parameters.FLASH_MODE_OFF
                }
    }
}