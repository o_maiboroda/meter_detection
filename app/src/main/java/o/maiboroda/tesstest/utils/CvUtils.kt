package o.maiboroda.tesstest.utils

import org.opencv.core.Rect

class CvUtils {
    private fun alignRects(boxes: ArrayList<Rect>): ArrayList<Rect> {
        val tmpRes = ArrayList<Rect>()
        tmpRes.clear()
        var averageWidth = 0.0
        var averageHeight = 0.0
        var averageY = 0.0

        boxes.forEach {
            averageWidth += it.width
            averageHeight += it.height
            averageY += it.y
        }
        averageWidth /= boxes.size
        averageHeight /= boxes.size
        averageY /= boxes.size

        if (boxes.size > 0)
            for (a in 0 until boxes.size) {
                val rect1 = boxes[a]
                var isInside = false
                for (b in 0 until boxes.size) {
                    val rect2 = boxes[b]
                    if (a == b)
                        break

                    //is near
                    if ((Math.abs(rect1.x - rect2.x) < rect1.width / 2.0))// && (rect1 != rect2) )
                    {
                        isInside = true
                    }
                }
                if (!isInside) {
                    //check width and hehigt with average values
                    if ((Math.abs(rect1.width - averageWidth) < (averageWidth / 2.0)) &&
                            (Math.abs(rect1.height - averageHeight) < (averageHeight / 2.0)) &&
                            (Math.abs(rect1.y - averageY) < (averageHeight / 2.0))) {
                        tmpRes.remove(rect1)
                    }
                }
            }

        return tmpRes
    }
}