package o.maiboroda.tesstest

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import android.widget.CompoundButton
import android.widget.NumberPicker
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.withContext
import o.maiboroda.tesstest.detector.Detector
import o.maiboroda.tesstest.imgproc.ImageProcessor
import o.maiboroda.tesstest.recognizer.Recognizer
import org.opencv.android.CameraBridgeViewBase
import org.opencv.core.Mat
import org.opencv.core.Rect
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timerTask


class MainActivity : AppCompatActivity(), CameraBridgeViewBase.CvCameraViewListener2 {
    private val recognizer by lazy { Recognizer(this) }
    private val detector by lazy { Detector(heightRange, minWidth) }
    private val imgProcessor by lazy { ImageProcessor() }

    //this area with red border
    private var resultMat: Mat? = null
    // this area is area above + border - to avoid out of range crash
    private var resultMatBorders: Mat? = null
    //all rects with digit from resultMat
    private var rectangles = ArrayList<Rect>()

    //digits height range
    private val heightRange by lazy {
        val metrics = DisplayMetrics()
        (getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.getMetrics(metrics)
        val screenHeight = metrics.heightPixels
        (screenHeight / 13)..(screenHeight / 8)
    }
    //digit min width
    private val minWidth by lazy { heightRange.first / 4 }

    private var counter = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recognizer.subscribe()

        cvCamera.setCvCameraViewListener(this)

        flashlight.setOnCheckedChangeListener { _: CompoundButton, b: Boolean ->
            cvCamera.enableFlashlight(b)
        }

        Timer().schedule(timerTask {
            System.gc()
            System.runFinalization()
        }, 5000, 5000)
    }

    override fun onCameraViewStarted(width: Int, height: Int) {}

    override fun onCameraViewStopped() {}

    override fun onCameraFrame(inputFrame: CameraBridgeViewBase.CvCameraViewFrame?): Mat? {
        counter++
        if (counter > 20) {
            counter = 0
            detector.clearCounter()
            rectangles.clear()
        }

        val mRgba = inputFrame?.rgba()!!
        //just src value of camera frame
        val srcCopy = Mat()
        mRgba.copyTo(srcCopy)

        val rect = imgProcessor.drawRectangle(mRgba, srcCopy, border) { mat, matBorder ->
            resultMat = mat
            resultMatBorders = matBorder
        }

        resultMat?.let { mat ->
            val invert = imgProcessor.preprocessImage(mat)

            detector.detect(invert, rectangles) { dsn -> shotImage(resultMatBorders, dsn) }
            detector.drawDetection(mRgba, rect, rectangles)
        }

        return mRgba
    }

    private fun shotImage(resultMat: Mat?, rectangles: MutableList<Rect>) = async(UI) {
        resultMat?.let {
            cvCamera.disableView()
            val src = imgProcessor.preprocessImage(it)
            showProgress(true)
            val text = withContext(CommonPool) { recognizer.recognize(src, rectangles, border) }
            showProgress(false)
            showPopup(text)
        }
    }

    private fun showPopup(text: ArrayList<Int>) {
        val view = layoutInflater.inflate(R.layout.dialog_meter, null)
        arrayOf(R.id.one, R.id.two, R.id.three, R.id.four, R.id.five).forEachIndexed { index, i ->
            view.findViewById<NumberPicker>(i).apply {
                minValue = 0
                maxValue = 9
                value = text[index]
            }
        }
        AlertDialog.Builder(this)
                .setView(view)
                .setCancelable(true)
                .setPositiveButton("Ok") { dialog, which ->
                    cvCamera.enableView()
                    dialog.dismiss()
                }
                .setNegativeButton("Cancel") { dialog, which ->
                    dialog.dismiss()
                    cvCamera.enableView()
                }
                .show()
    }

    private fun showProgress(isProgress: Boolean) {
        progress.visibility = if (isProgress) View.VISIBLE else View.GONE
    }

    override fun onResume() {
        super.onResume()
        cvCamera.enableView()
    }

    companion object {
        init {
            System.loadLibrary("opencv_java3")
            System.loadLibrary("native-lib")
        }

        private const val border = 100

        private const val TAG = "tag"
    }
}