package o.maiboroda.tesstest.detector

import org.opencv.core.*
import org.opencv.imgproc.Imgproc

class Detector(private val heightRange: IntRange, private val minWidth: Int) {

    private var fullCounter = 0

    fun clearCounter() {
        fullCounter = 0
    }

    fun drawDetection(src: Mat, rect: Rect, rectangles: MutableList<Rect>) {
        rectangles.forEach {
            Imgproc.rectangle(src, Point(rect.tl().x + it.tl().x, rect.tl().y + it.tl().y),
                    Point(rect.tl().x + it.br().x, rect.tl().y + it.br().y),
                    Scalar(0.0, 255.0, 0.0), 2, 8, 0)
        }
    }

    fun detect(src: Mat, rectangles: MutableList<Rect>, callback: (dsn: ArrayList<Rect>) -> Unit) {
        val canny = Mat()
        Imgproc.Canny(src, canny, 100.0, 200.0)

        val contours = ArrayList<MatOfPoint>()
        val hierarcy = Mat()
        Imgproc.findContours(canny, contours, hierarcy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE)

        //draw results of character detection - green rects.
        // add some space around for better recognizing
        val size = 10
        var averageY = 0.0
        val newRects = filterContours(contours)
        newRects.forEach {
            it.x = it.x - size
            it.y = it.y - size
            it.height = it.height + size * 2
            it.width = it.width + size * 2
            averageY += it.y
        }
        averageY /= newRects.size
        newRects.filter {
            (it.y - averageY) < it.height / 2
        }

        if (checkSpace(newRects))
            checkIfFull(rectangles, newRects, callback)

        val filteredRects = newRects.filter {
            var boo = true
            val rangeX = (it.x - it.width / 2)..(it.x + it.width / 2)
            rectangles.forEachIndexed { index, rect ->
                if (rect.x in rangeX) {
                    rectangles[index] = it
                    boo = false
                }
            }
            boo
        }

        rectangles.addAll(filteredRects)
    }

    private fun filterContours(contours: ArrayList<MatOfPoint>): ArrayList<Rect> {
        val rects = ArrayList<Rect>()
        contours.forEach {
            val bounds = Imgproc.boundingRect(it)
            if (bounds.height in heightRange && bounds.width > minWidth && bounds.width < bounds.height)
                rects.add(bounds)
        }
        return rects
    }

    private fun checkIfFull(src: MutableList<Rect>, dsn: ArrayList<Rect>, callback: (dsn: ArrayList<Rect>) -> Unit) {
        if (src.size > 5 && src.size == dsn.size) {
            src.forEachIndexed { index, rect1 ->
                val rangeX = (rect1.x - rect1.width / 2)..(rect1.x + rect1.width / 2)
                val rect2 = dsn[index]
                if (rect2.x in rangeX) {
                    fullCounter++
                    if (fullCounter > 10) {
                        fullCounter = 0
                        callback(dsn)
                    }
                }
            }
        }
    }

    private fun checkSpace(rects: MutableList<Rect>): Boolean {
        rects.sortBy { it.x }

        for (i in 0 until rects.size - 2) {
            val rect = rects[i]
            if ((rects[i + 1].x - rect.x) > rect.height * 2)
                return false
        }
        return true
    }
}