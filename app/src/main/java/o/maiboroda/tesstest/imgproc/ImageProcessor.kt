package o.maiboroda.tesstest.imgproc

import org.opencv.core.*
import org.opencv.imgproc.Imgproc

class ImageProcessor {

    fun preprocessImage(srcMat: Mat): Mat {
        val gray = Mat(srcMat.size(), CvType.CV_8UC1)
        Imgproc.cvtColor(srcMat, gray, Imgproc.COLOR_RGB2GRAY)

        Imgproc.GaussianBlur(gray, gray, Size(5.0, 5.0), 0.0)

        Imgproc.medianBlur(gray, gray, 5)

        val binary = Mat()
//        Imgproc.adaptiveThreshold(gray, binary, 255.0, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
//                Imgproc.THRESH_BINARY, 11, 2.0)
//        Imgproc.threshold(gray, binary, 150.0, 255.0, Imgproc.THRESH_OTSU)

//        Imgproc.threshold(gray, binary, 128.0, 255.0, Imgproc.THRESH_BINARY)
        Imgproc.threshold(gray, binary, 128.0, 255.0, Imgproc.THRESH_BINARY_INV)

        return binary
    }

    fun drawRectangle(mRgba: Mat, src: Mat, border: Int, callback: (mat: Mat, matBorder: Mat) -> Unit): Rect {
        val rect = Rect()
        rect.width = mRgba.width()
        rect.height = mRgba.height()

        val height = (mRgba.height() * 0.25).toInt()
        val width = (mRgba.width() * 0.7).toInt()
        val x = (rect.tl().x + rect.br().x).toInt() / 2
        val y = (rect.tl().y + rect.br().y).toInt() / 2

        val width2 = width + border
        val height2 = height + border

        val rect1 = Rect(x - width / 2, y - height / 2, width, height)
        val rect2 = Rect(x - width2 / 2, y - height2 / 2, width2, height2)
        callback(Mat(src, rect1), Mat(src, rect2))

        Imgproc.rectangle(mRgba, rect1.tl(), rect1.br(), Scalar(255.0, 0.0, 0.0), 2, 8, 0)

        return rect1
    }
}